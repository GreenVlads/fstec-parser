﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using OfficeOpenXml;

namespace FSTECParser
{
    public partial class Form1 : Form
    {
        public string filePath = "ThreatListFSTEC.xlsx";
        public string UPDfilePath = "UPDThreatListFSTEC.xlsx";
        public string reportFilePath = "UpdateReport.txt";
        internal static BindingList<Threat> threats = new BindingList<Threat>();
        internal static BindingList<Threat> UPDThreats = new BindingList<Threat>();
        internal static BindingList<Threat> sheetThreats = new BindingList<Threat>();
        public static int currentPage;
        public static int MaxPage;

        public Form1()
        {
            InitializeComponent();
            currentPage = 1;
            listBox1.DataSource = sheetThreats;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!File.Exists(filePath))
            {
                try
                {
                    new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", filePath);
                }
                catch (System.Net.WebException x)
                {
                    MessageBox.Show(x.Message + "\nДанный ресурс недоступен");
                }
            }
            else
            {
                MessageBox.Show("Файл уже существует");
            }
        }


        static private void ParseThreats(string filename)
        {
            using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(filename)))
            {
                var myWorksSheet = xlPackage.Workbook.Worksheets.First();
                var totalRows = myWorksSheet.Dimension.End.Row;
                var totalColumns = myWorksSheet.Dimension.End.Column;

                for (int rowNum = 3; rowNum <= totalRows; rowNum++)
                {
                    var row = myWorksSheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                    threats.Add(MakeThreat(row.ToArray<string>())); 
                }
            }
            MaxPage = (threats.Count / 15) + 1;
        }

        private static Threat MakeThreat(string[] strings)
        {
            return new Threat(int.Parse(strings[0]), strings[1], strings[2], strings[3], strings[4],
                strings[5] == "1" ? true : false, strings[5] == "1" ? true : false, strings[5] == "1" ? true : false);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                threats.Clear();
                sheetThreats.Clear();
                ParseThreats(filePath);
                for (int i = 15 * (currentPage - 1); i < 15 * (currentPage - 1) + 15; i++)
                {
                    if (i >= threats.Count)
                    {
                        break;
                    }
                    sheetThreats.Add(threats[i]);
                }
                PagetextBox.Text = currentPage.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("Файл пока что не скачан.\nВы можете скачать его, нажав на кнопку Donwload");
            }
        }

        private void listBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Point click = e.Location;
            int ThreatIndex = listBox1.IndexFromPoint(click);
            try
            {
                MessageBox.Show("УБИ." + sheetThreats[ThreatIndex].Id.ToString() + "\n\n" +
                    "Наименование УБИ: " + sheetThreats[ThreatIndex].ThreatName.ToString() + "\n\n" +
                    "Описание: " + sheetThreats[ThreatIndex].ThreatDescription.ToString().Replace("_x000d_", "") + "\n\n" +
                    "Источник угрозы: " + sheetThreats[ThreatIndex].ThreatSource.ToString() + "\n\n" +
                    "Объект воздействия: " + sheetThreats[ThreatIndex].ThreatObject.ToString() + "\n\n" +
                    "Нарушение конфиденциальности: " + sheetThreats[ThreatIndex].Confidentiality.ToString() + "\n" +
                    "Нарушение целостности: " + sheetThreats[ThreatIndex].Integrity.ToString() + "\n" +
                    "Нарушение доступности: " + sheetThreats[ThreatIndex].Availability.ToString(), "Информация о записи");
            }
            catch (Exception)
            {
                // void
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void справкаToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Здравствуйте, добро пожаловать в приложение.\n" +
                "В этом приложении вы можете загружать, обновлять и\n" +
                "просматривать записи об угрозах безопасности информации,\n" + 
                "с банка данных угроз безопасности информации ФСТЭК России.\n\n" +
                "Для начала работы вы должны скачать файл, нажав на кнопку \"Download\".\n" +
                "Затем для вывода и просмотра информации в окне необходимо нажать на кнопку \"Parse\".\n" + 
                "Для обновления информации необходимо нажать на кнопку \"Update\".", "Справка");
        }

        private void LeftButton_MouseClick(object sender, MouseEventArgs e)
        {
            if ((currentPage - 1) >= 1)
            {
                sheetThreats.Clear();
                currentPage--;
                for (int i = 15 * (currentPage - 1); i < 15 * (currentPage - 1) + 15; i++)
                {
                    sheetThreats.Add(threats[i]);
                }
                PagetextBox.Text = currentPage.ToString();
            }
        }

        private void RightButton_MouseClick(object sender, MouseEventArgs e)
        {
            if ((currentPage + 1) <= MaxPage)
            {
                sheetThreats.Clear();
                currentPage++;
                for (int i = 15 * (currentPage - 1); i < 15 * (currentPage - 1) + 15; i++)
                {
                    try
                    {
                        sheetThreats.Add(threats[i]);
                    }
                    catch (Exception)
                    {
                        break;
                    }
                }
                PagetextBox.Text = currentPage.ToString();
                
            }
        }

        private void updButton_MouseClick(object sender, MouseEventArgs e)
        {
            if (threats.Count != 0)
            {
                List<int> id = new List<int>();
                int UpdatedThreatsCount = 0;
                try
                {
                    new WebClient().DownloadFile("https://bdu.fstec.ru/documents/files/thrlist.xlsx", UPDfilePath);
                    using (ExcelPackage xlPackage = new ExcelPackage(new FileInfo(UPDfilePath)))
                    {
                        var myWorksSheet = xlPackage.Workbook.Worksheets.First();
                        var totalRows = myWorksSheet.Dimension.End.Row;
                        var totalColumns = myWorksSheet.Dimension.End.Column;

                        for (int rowNum = 3; rowNum <= totalRows; rowNum++)
                        {
                            var row = myWorksSheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                            UPDThreats.Add(MakeThreat(row.ToArray<string>()));
                        }
                    }
                    MaxPage = (UPDThreats.Count / 15) + 1;
                    foreach (Threat updThreat in UPDThreats)
                    {
                        if (!(threats.Contains(updThreat)))
                        {
                            UpdatedThreatsCount++;
                            id.Add(updThreat.Id);
                        }
                    }
                    using (FileStream report = new FileStream(reportFilePath, FileMode.Create))
                    {
                        using (StreamWriter reportFile = new StreamWriter(report))
                        {
                            reportFile.WriteLine("Общее количество обновленных записей: " + UpdatedThreatsCount);
                            reportFile.WriteLine("Было:");
                            foreach (int idValue in id)
                            {
                                if (idValue <= threats.Count)
                                {
                                    reportFile.WriteLine("Id: " + threats[idValue - 1].Id);
                                    reportFile.WriteLine("Наименование УБИ: " + threats[idValue - 1].ThreatName.ToString());
                                    reportFile.WriteLine("Описание: " + threats[idValue - 1].ThreatDescription.ToString().Replace("_x000d_", ""));
                                    reportFile.WriteLine("Источник угрозы: " + threats[idValue - 1].ThreatSource.ToString());
                                    reportFile.WriteLine("Объект воздействия: " + threats[idValue - 1].ThreatObject.ToString());
                                    reportFile.WriteLine("Нарушение конфиденциальности: " + threats[idValue - 1].Confidentiality.ToString());
                                    reportFile.WriteLine("Нарушение целостности: " + threats[idValue - 1].Integrity.ToString());
                                    reportFile.WriteLine("Нарушение доступности: " + threats[idValue - 1].Availability.ToString(), "Информация о записи");
                                }
                            }
                            reportFile.WriteLine("Стало:");
                            foreach (int idValue in id)
                            {
                                reportFile.WriteLine("Id: " + idValue);
                                reportFile.WriteLine("Наименование УБИ: " + UPDThreats[idValue - 1].ThreatName.ToString());
                                reportFile.WriteLine("Описание: " + UPDThreats[idValue - 1].ThreatDescription.ToString().Replace("_x000d_", ""));
                                reportFile.WriteLine("Источник угрозы: " + UPDThreats[idValue - 1].ThreatSource.ToString());
                                reportFile.WriteLine("Объект воздействия: " + UPDThreats[idValue - 1].ThreatObject.ToString());
                                reportFile.WriteLine("Нарушение конфиденциальности: " + UPDThreats[idValue - 1].Confidentiality.ToString());
                                reportFile.WriteLine("Нарушение целостности: " + UPDThreats[idValue - 1].Integrity.ToString());
                                reportFile.WriteLine("Нарушение доступности: " + UPDThreats[idValue - 1].Availability.ToString(), "Информация о записи");
                            }
                            MessageBox.Show("Успешно обновлено!\nПосмотрите отчет в UpdateReport.txt файле");
                        }
                    }

                    UPDThreats.Clear();
                    

                    try
                    {
                        threats.Clear();
                        sheetThreats.Clear();
                        ParseThreats(UPDfilePath);
                        for (int i = 15 * (currentPage - 1); i < 15 * (currentPage - 1) + 15; i++)
                        {
                            if (i >= threats.Count)
                            {
                                break;
                            }
                            sheetThreats.Add(threats[i]);
                        }
                        PagetextBox.Text = currentPage.ToString();
                        filePath = UPDfilePath;
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Файл пока что не скачан.\nВы можете скачать его, нажав на кнопку Donwload");
                    }

                }
                catch (System.Net.WebException x)
                {
                    MessageBox.Show(x.Message + "\nДанный ресурс недоступен");
                }
            }
        }

    }
}
